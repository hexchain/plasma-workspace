msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-02 02:04+0000\n"
"PO-Revision-Date: 2023-09-16 10:05\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-workspace/krunner.pot\n"
"X-Crowdin-File-ID: 43549\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国, EasternLi, fusionfuture, Guo Yunhe, Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"kde-china@kde.org, EasternLi@kde.org, fusionfuture@kde.org, i@guoyunhe.me, "
"tysontan@tysontan.com"

#: main.cpp:77 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:77
#, kde-format
msgid "Run Command interface"
msgstr "运行命令界面"

#: main.cpp:83
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "使用剪贴板内容作为 KRunner 查询条件"

#: main.cpp:84
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "启动 KRunner 到后台，不显示界面。"

#: main.cpp:85
#, kde-format
msgid "Replace an existing instance"
msgstr "替换已有实例"

#: main.cpp:86
#, kde-format
msgid "Show only results from the given plugin"
msgstr "只显示指定插件的结果"

#: main.cpp:87
#, kde-format
msgid "List available plugins"
msgstr "列出可用插件"

#: main.cpp:94
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "要运行的查询，只在未提供 -c 参数时使用"

#: main.cpp:103
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr "可用的 KRunner 插件，插件 ID"

#: qml/RunCommand.qml:94
#, kde-format
msgid "Configure"
msgstr "配置"

#: qml/RunCommand.qml:95
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "配置 KRunner 行为"

#: qml/RunCommand.qml:98
#, kde-format
msgid "Configure KRunner…"
msgstr "配置 KRunner…"

#: qml/RunCommand.qml:111
#, kde-format
msgid "Showing only results from %1"
msgstr "只显示来自 %1 的结果"

#: qml/RunCommand.qml:125
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "搜索“%1”…"

#: qml/RunCommand.qml:126
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "搜索…"

#: qml/RunCommand.qml:297 qml/RunCommand.qml:298 qml/RunCommand.qml:300
#, kde-format
msgid "Show Usage Help"
msgstr "显示使用方式帮助"

#: qml/RunCommand.qml:308
#, kde-format
msgid "Pin"
msgstr "固定"

#: qml/RunCommand.qml:309
#, kde-format
msgid "Pin Search"
msgstr "固定搜索"

#: qml/RunCommand.qml:311
#, kde-format
msgid "Keep Open"
msgstr "保持打开"

#: qml/RunCommand.qml:383 qml/RunCommand.qml:388
#, kde-format
msgid "Recent Queries"
msgstr "最近查询"

#: qml/RunCommand.qml:386
#, kde-format
msgid "Remove"
msgstr "移除"
