# translation of ksmserver.po to Kazakh
#
# Sairan Kikkarin <sairan@computer.org>, 2005, 2007, 2008, 2010.
# Sairan Kikkarin <sairan@computer.org>, 2010, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2013-01-02 06:41+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"\n"
"\n"
"\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "'%1' шығуды доғарды"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:193
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"X11R6 жүйесінің стандартты сеанс басқару протоколын (XSMP)\n"
"қолданатын сенімді KDE сеанс менеджері."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Мүмкіндік болса, пайдаланушының сақталған сеансын қалпына келтіру"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "Қашық қосылымдарды да рұқсат етілсін"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr "Сеансты бұғаталған күйде бастау"

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:881
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "KDE сеанс менеджері"

#: server.cpp:886
#, kde-format
msgid "Log Out"
msgstr "Шығып кету"

#: server.cpp:891
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:896
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:902
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Құптаусыз шығып кету"

#: server.cpp:907
#, fuzzy, kde-format
#| msgid "Halt Without Confirmation"
msgid "Shut Down Without Confirmation"
msgstr "Құптаусыз тоқтату"

#: server.cpp:912
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Құптаусыз қайта жүктеу"

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Басқа терезе менеджері қолдануда болмаса, 'wm' менеджерін\n"
#~ "іске қосу. Әдетте 'kwin' қолданылады"

#, fuzzy
#~| msgid "&Logout"
#~ msgid "Logout"
#~ msgstr "Жүйден &шығу"

#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "%1 cекундтан кейін қалғу."

#~ msgid "Lock"
#~ msgstr "Бұғатау"

#~ msgid "Turn off"
#~ msgstr "Сөндіру"

#~ msgid "Sleep"
#~ msgstr "Қалғу"

#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "%1 cекундтан кейін шығу."

#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Компьютерді %1 секундта сөндіру."

#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Компьютерді %1 секундта қайта жүктеу."

#, fuzzy
#~| msgid "&Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "Компьютерді &сөндіру"

#, fuzzy
#~| msgid "&Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "Компьютерді қайта жегу"

#~ msgctxt "default option in boot loader"
#~ msgid " (default)"
#~ msgstr " (әдетті)"

#, fuzzy
#~| msgid "&Cancel"
#~ msgid "Cancel"
#~ msgstr "Қа&йту"

#~ msgid "&Standby"
#~ msgstr "&Күту режімі"

#~ msgid "Suspend to &RAM"
#~ msgstr "Ж&адына сақталып аялдау"

#~ msgid "Suspend to &Disk"
#~ msgstr "&Дискіне сақталып аялдау"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Сайран Киккарин"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sairan@computer.org"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) 2000, KDE жасаушылар"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matthias Ettrich"

#~ msgid "Luboš Luňák"
#~ msgstr "Luboš Luňák"

#~ msgid "Maintainer"
#~ msgstr "Жетелдірушісі"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " (қолданудағы)"
