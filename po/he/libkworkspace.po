# translation of libkworkspace.po to hebrew
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# tahmar1900 <tahmar1900@gmail.com>, 2008.
# Diego Iastrubni <elcuco@kde.org>, 2008, 2012.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: libkworkspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2017-05-16 06:54-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Zanata 3.9.6\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kdisplaymanager.cpp:770
#, fuzzy, kde-format
#| msgctxt "user: ..."
#| msgid "%1: TTY login"
msgctxt "user: …"
msgid "%1: TTY login"
msgstr "%1: התחברות מסוף"

#: kdisplaymanager.cpp:775
#, fuzzy, kde-format
#| msgctxt "... location (TTY or X display)"
#| msgid "Unused"
msgctxt "… location (TTY or X display)"
msgid "Unused"
msgstr "לא בשימוש"

#: kdisplaymanager.cpp:776
#, kde-format
msgid "X login on remote host"
msgstr "התחברות גרפית משרת מרוחק"

#: kdisplaymanager.cpp:776
#, fuzzy, kde-format
#| msgctxt "... host"
#| msgid "X login on %1"
msgctxt "… host"
msgid "X login on %1"
msgstr "חיבור גרפי ב־%1"

#: kdisplaymanager.cpp:778
#, kde-format
msgctxt "user: session type"
msgid "%1: %2"
msgstr "%1: %2"

#: kdisplaymanager.cpp:789
#, kde-format
msgctxt "session (location)"
msgid "%1 (%2)"
msgstr "%1 (%2)"
