# translation of plasma_wallpaper_image.po to Icelandic
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2009, 2010, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-21 01:57+0000\n"
"PO-Revision-Date: 2023-09-15 10:13+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Guðmundur Erlingsson"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gudmundure@gmail.com"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, kde-format
msgctxt "@title:window"
msgid "Open Image"
msgstr "Opna mynd"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, kde-format
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Mappa með veggfóðursmyndum sem á að sýna sem skyggnusýningu"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Staðsetning:"

#: imagepackage/contents/ui/config.qml:103
#, kde-format
msgid "Scaled and Cropped"
msgstr "Fylla skjá og skera af"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Fylla skjá"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Fylla skjá, halda hlutföllum"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "Miðja"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Flísaleggja"

#: imagepackage/contents/ui/config.qml:147
#, kde-format
msgid "Background:"
msgstr "Bakgrunnur:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr "Móða"

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr "Hreinn litur"

#: imagepackage/contents/ui/config.qml:167
#, kde-format
msgid "Select Background Color"
msgstr "Velja bakgrunnslit"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Opna veggfóðursmynd"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Næsta veggfóðursmynd"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, kde-format
msgid "Images"
msgstr "Myndir"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr "Bæta við…"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, kde-format
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Ná í nýtt..."

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Opna möppu sem skráin er í"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Endurheimta veggfóður"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Fjarlægja veggfóður"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""
"Með þessu verkfæri getur þú valið mynd sem veggfóður fyrir Plasma-setuna."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Myndskrá eða uppsettur kpackage-veggfóðurspakki sem þú vilt velja sem "
"veggfóður fyrir Plasma-setuna"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"Það er stök einföld gæsalöpp í skrárheiti veggfóðursins ('). Hafðu samband "
"við höfund veggfóðursins til að laga þetta, eða gefðu skránni nýtt heiti: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "Villa kom upp þegar reynt var að velja Plasma-veggfóður:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr "Tókst að stilla veggfóður fyrir öll skjáborð á KPackage-pakkann %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "Tókst að stilla veggfóður allra skjáborða á myndina %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"Skráin sem átti að velja sem veggfóður er ekki til, eða við getum ekki séð "
"að hún geti verið veggfóður: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Myndskrár"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr "Röðun:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr "Slembið"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr "A til Ö"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr "Ö til A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr "Breytingardagsetning (nýjasta fyrst)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Breytingardagsetning (elsta fyrst)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr "Flokka eftir möppum"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Skipta eftir:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 klukkustund"
msgstr[1] "%1 klukkustundir"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 mínútu"
msgstr[1] "%1 mínútur"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekúndu"
msgstr[1] "%1 sekúndur"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, kde-format
msgid "Folders"
msgstr "Möppur"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr "Bæta við…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:223
#, kde-format
msgid "Remove Folder"
msgstr "Fjarlægja möppu"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:228
#, kde-format
msgid "Open Folder…"
msgstr "Opna möppu…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:238
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "Engar staðsetningar veggfóðurs eru stilltar"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Velja sem veggfóður"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Skjáborð"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Læsiskjár"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Bæði"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr "Villa kom upp þegar reynt var að velja Plasma-veggfóður:<nl/>%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""
"Óvænt villa kom upp þegar reynt var að opna kscreenlockerrc "
"grunnstillingaskrá."

#~ msgid "Add Image…"
#~ msgstr "Bæta við mynd…"

#~ msgid "Add Folder…"
#~ msgstr "Bæta við möppu…"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Veggfóðursskrá sem mælt er með"

#, fuzzy
#~| msgid "Get New Wallpapers..."
#~ msgid "Add Custom Wallpaper"
#~ msgstr "Ná í ný veggfóður..."

#, fuzzy
#~| msgid "Get New Wallpapers..."
#~ msgid "Remove wallpaper"
#~ msgstr "Ná í ný veggfóður..."

#, fuzzy
#~| msgctxt "Caption to wallpaper preview, %1 author name"
#~| msgid "by %1"
#~ msgid "%1 by %2"
#~ msgstr "eftir %1"

#, fuzzy
#~| msgid "Get New Wallpapers..."
#~ msgid "Wallpapers"
#~ msgstr "Ná í ný veggfóður..."

#, fuzzy
#~| msgid "Download new wallpapers"
#~ msgid "Download Wallpapers"
#~ msgstr "Hala niður nýjum veggfóðrum"

#~ msgid "Open..."
#~ msgstr "Opna..."

#~ msgid "Center Tiled"
#~ msgstr "Flísalagt frá miðju"

#~ msgid "Select Wallpaper Image File"
#~ msgstr "Veldu myndskrá í bakgrunn (veggfóður)"

#~ msgid "P&ositioning:"
#~ msgstr "S&taðsetning:"

#~ msgid "&Color:"
#~ msgstr "&Litur:"

#~ msgid "Remove from list"
#~ msgstr "Fjarlægja úr lista"

#~ msgctxt ""
#~ "(qtdt-format) Please do not change the quotes (') and translate only the "
#~ "content of the quotes."
#~ msgid "hh 'Hours' mm 'Mins' ss 'Secs'"
#~ msgstr "hh 'klst' mm 'mín' ss 'sek'"

#, fuzzy
#~| msgid "Download new wallpapers"
#~ msgid "&My downloaded wallpapers:"
#~ msgstr "Hala niður nýjum veggfóðrum"

#~ msgid "Finding images for the wallpaper slideshow."
#~ msgstr "Finn myndir fyrir bakgrunnssýningu (veggfóður)."

#~ msgid "Adding wallpaper package in %1"
#~ msgstr "Bæti við veggfóðurpakka í %1"

#~ msgid "Adding image %1"
#~ msgstr "Bæti við mynd %1"
