# translation of krunner_powerdevil.po to Oriya
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manoj Kumar Giri <mgiri@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: krunner_powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-09 01:58+0000\n"
"PO-Revision-Date: 2009-01-02 11:48+0530\n"
"Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword; 'power' as in 'power saving mode'"
msgid "power"
msgstr ""

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr ""

#: PowerDevilRunner.cpp:37
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr ""

#: PowerDevilRunner.cpp:39
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr ""

#: PowerDevilRunner.cpp:41
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr ""

#: PowerDevilRunner.cpp:43
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr ""

#: PowerDevilRunner.cpp:45
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid sleep"
msgstr ""

#: PowerDevilRunner.cpp:47
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid"
msgstr ""

#: PowerDevilRunner.cpp:49 PowerDevilRunner.cpp:51 PowerDevilRunner.cpp:76
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr ""

#: PowerDevilRunner.cpp:60
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""

#: PowerDevilRunner.cpp:64
#, kde-format
msgid "Suspends the system to RAM"
msgstr ""

#: PowerDevilRunner.cpp:68
#, kde-format
msgid "Suspends the system to disk"
msgstr ""

#: PowerDevilRunner.cpp:72
#, kde-format
msgid "Sleeps now and falls back to hibernate"
msgstr ""

#: PowerDevilRunner.cpp:75
#, kde-format
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr ""

#: PowerDevilRunner.cpp:78
#, no-c-format, kde-format
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""

#: PowerDevilRunner.cpp:100
#, kde-format
msgid "Set Brightness to %1%"
msgstr ""

#: PowerDevilRunner.cpp:109
#, kde-format
msgid "Dim screen totally"
msgstr ""

#: PowerDevilRunner.cpp:117
#, kde-format
msgid "Dim screen by half"
msgstr ""

#: PowerDevilRunner.cpp:146
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr ""

#: PowerDevilRunner.cpp:147
#, kde-format
msgid "Suspend to RAM"
msgstr ""

#: PowerDevilRunner.cpp:152
#, kde-format
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr ""

#: PowerDevilRunner.cpp:153
#, kde-format
msgid "Suspend to disk"
msgstr ""

#: PowerDevilRunner.cpp:158
#, kde-format
msgctxt "Suspend to both RAM and disk"
msgid "Hybrid sleep"
msgstr ""

#: PowerDevilRunner.cpp:159
#, kde-format
msgid "Sleep now and fall back to hibernate"
msgstr ""

#: PowerDevilRunner.cpp:237
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr ""

#: PowerDevilRunner.cpp:239
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr ""
