# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Vit Pelcak <vit@pelcak.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-04 02:00+0000\n"
"PO-Revision-Date: 2023-08-22 10:00+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.3\n"

#: kcm_soundtheme.cpp:107
#, kde-format
msgctxt "Name of the fallback \"freedesktop\" sound theme"
msgid "FreeDesktop"
msgstr "FreeDesktop"

#: kcm_soundtheme.cpp:108
#, kde-format
msgid "Fallback sound theme from freedesktop.org"
msgstr ""

#: ui/main.qml:59
#, kde-format
msgctxt ""
"%2 is a theme name or a list of theme names that the current theme inherits "
"from"
msgid "Based on: %2"
msgid_plural "Based on: %2"
msgstr[0] ""
msgstr[1] ""

#: ui/main.qml:70
#, kde-format
msgctxt ""
"@label Precedes a list of buttons which can be clicked to preview the "
"theme's sounds. Keep it short"
msgid "Preview sounds:"
msgstr ""

#: ui/main.qml:84
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview sound \"%1\""
msgstr ""

#: ui/main.qml:106
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview the demo sound for the theme \"%1\""
msgstr ""
