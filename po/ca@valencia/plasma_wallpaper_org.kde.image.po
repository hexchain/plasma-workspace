# Translation of plasma_wallpaper_org.kde.image.po to Catalan (Valencian)
# Copyright (C) 2007-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2 or later.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2007, 2008, 2009, 2010, 2011, 2013, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Orestes Mas Casals <orestes@tsc.upc.edu>, 2008.
# Robert Millan <rmh@aybabtu.com>, 2009.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-21 01:57+0000\n"
"PO-Revision-Date: 2023-09-15 10:47+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, kde-format
msgctxt "@title:window"
msgid "Open Image"
msgstr "Obriu una imatge"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, kde-format
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr ""
"Directori amb el fons de pantalla des del qual mostrar les diapositives"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Posicionament:"

#: imagepackage/contents/ui/config.qml:103
#, kde-format
msgid "Scaled and Cropped"
msgstr "Escalat i escapçat"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Escalat"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Escalat, mantenint les proporcions"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "Centrat"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Mosaic"

#: imagepackage/contents/ui/config.qml:147
#, kde-format
msgid "Background:"
msgstr "Fons:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr "Difuminat"

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr "Color sòlid"

#: imagepackage/contents/ui/config.qml:167
#, kde-format
msgid "Select Background Color"
msgstr "Seleccioneu el color de fons"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Obri imatge de fons de pantalla"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Fons de pantalla següent"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, kde-format
msgid "Images"
msgstr "Imatges"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr "Afig…"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, kde-format
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Obtín noves…"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Obri la carpeta contenidora"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Restaura el fons de pantalla"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Elimina el fons de pantalla"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""
"Esta eina permet definir una imatge com a fons de pantalla per a la sessió "
"de Plasma."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Un fitxer d'imatge o un «kpackage» instal·lat de fons de pantalla que "
"vulgueu definir com a fons de pantalla per a la sessió de Plasma"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"Hi ha una cometa simple extraviada en el nom de fitxer d'este fons de "
"pantalla (') - contacteu amb l'autor dels fons de pantalla perquè ho "
"corregisca, o canvieu de nom el fitxer: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr ""
"S'ha produït un error en intentar establir el fons de pantalla de Plasma:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""
"S'ha definit correctament el fons de pantalla per a tots els escriptoris amb "
"%1 basat en KPackage"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr ""
"S'ha definit correctament el fons de pantalla per a tots els escriptoris amb "
"la imatge %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"El fitxer passat per a definir com a fons de pantalla no existix, o no s'ha "
"pogut identificar com a fons de pantalla: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Fitxers d'imatge"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr "Ordre:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr "Aleatori"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr "A a Z"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr "Z a A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr "Data de modificació (primer els més nous)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Data de modificació (primer els més antics)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr "Agrupa per carpetes"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Canvia cada:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hora"
msgstr[1] "%1 hores"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minuts"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segon"
msgstr[1] "%1 segons"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, kde-format
msgid "Folders"
msgstr "Carpetes"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr "Afig…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:223
#, kde-format
msgid "Remove Folder"
msgstr "Elimina la carpeta"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:228
#, kde-format
msgid "Open Folder…"
msgstr "Obri una carpeta…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:238
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "No s'ha configurat cap ubicació de fons de pantalla"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Establix com a fons de pantalla"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Escriptori"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Pantalla de bloqueig"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Ambdós"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr ""
"S'ha produït un error en intentar establir el fons de pantalla de Plasma:<nl/"
">%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""
"S'ha produït un error en intentar obrir el fitxer de configuració "
"«kscreenlockerrc»."
