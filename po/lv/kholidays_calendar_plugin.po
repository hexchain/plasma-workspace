# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-03 02:20+0000\n"
"PO-Revision-Date: 2020-07-31 11:09+0300\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 20.04.3\n"

#: HolidaysConfig.qml:89
#, kde-format
msgid "Region"
msgstr "Reģions"

#: HolidaysConfig.qml:93
#, kde-format
msgid "Name"
msgstr "Nosaukums"

#: HolidaysConfig.qml:97
#, kde-format
msgid "Description"
msgstr "Apraksts"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Meklēt..."
