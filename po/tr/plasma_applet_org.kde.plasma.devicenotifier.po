# translation of plasma_applet_devicenotifier.po to Turkish
# translation of plasma_applet_devicenotifier.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Serdar Soytetir <tulliana@gmail.com>, 2007, 2008, 2009, 2011.
# Serdar Soytetir <tulliana@gmail.com>, 2007, 2012.
# H. İbrahim Güngör <ibrahim@pardus.org.tr>, 2010.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2017, 2021, 2022.
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-30 01:58+0000\n"
"PO-Revision-Date: 2023-05-21 21:15+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.03.70\n"
"X-POOTLE-MTIME: 1413130721.000000\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1/%2 boş"

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Erişiliyor…"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Kaldırılıyor…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr "Henüz çıkarmayın! Dosyalar hâlâ aktarılıyor..."

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr "Dosya Yöneticisinde Aç"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr "Bağla ve Aç"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr "Çıkar"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr "Güvenli Kaldır"

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr "Bağla"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:253
#, kde-format
msgid "Remove All"
msgstr "Tümünü Kaldır"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Tüm aygıtları güvenle kaldırmak için tıklayın"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr "Takılı çıkarılabilir aygıt yok"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr "Kullanılabilir disk yok"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr "Son Takılan Aygıt"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr "Kullanılabilir Aygıt Yok"

#: package/contents/ui/main.qml:235
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Çıkarılabilir Aygıtları Yapılandır…"

#: package/contents/ui/main.qml:261
#, kde-format
msgid "Removable Devices"
msgstr "Çıkarılabilir Aygıtlar"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "Non Removable Devices"
msgstr "Çıkarılabilir Olmayan Aygıtlar"

#: package/contents/ui/main.qml:291
#, kde-format
msgid "All Devices"
msgstr "Tüm Aygıtlar"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Yeni Aygıt Takıldığında Açılır Pencere Göster"
